import React from 'react';
import ReactDOM from 'react-dom';
import Routing  from './router';

ReactDOM.render(<Routing /> , document.getElementById('root'));