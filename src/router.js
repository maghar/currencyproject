import React, { Fragment } from "react";
import { Router, Route, Redirect } from "react-router";
import { createBrowserHistory } from "history";

import App from "./pages/App";
import Login from "./pages/Login";
import HomePage from "./pages/HomePage";
import Registration from "./pages/Registration";

const history = createBrowserHistory();

const isLoggedIn = () => {
  return localStorage.getItem("TOKEN") ? true : false;
};

const routes = [
  {
    path: "/",
    component: App,
    routes: [
      {
        path: "/registration",
        component: Registration
      },
      {
        path: "/home",
        component: HomePage
      }
    ]
  }
];

const PrivateRoute = route => (
  <Route
    path={route.path}
    render={props =>
      isLoggedIn() ? (
        <route.component {...props} routes={route.routes} />
      ) : (
        <Redirect to="/login" />
      )
    }
  />
);

const Routing = () => {
  return (
    <Router history={history}>
      <Fragment>
        <Route exact path="/login" component={Login} />
        {routes.map(route => (
          <PrivateRoute key={route.path} {...route} />
        ))}
      </Fragment>
    </Router>
  );
};

export default Routing;
