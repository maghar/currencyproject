import React, { Component } from "react";

class Login extends Component {
  componentDidMount() {
    localStorage.setItem("TOKEN", "test");

    console.log(localStorage)
  }

  goHome = () => {
    if (localStorage.getItem("email") && localStorage.getItem("password")) {
      this.props.history.push("/home");
    } else {
        alert('Firstly register');
    }
  };

  reset = () => {
      localStorage.removeItem('user');
  }

  render() {
    return (
      <div className="page login-page">
        <h1>Login</h1>
        <button onClick={()=>this.props.history.push("/registration")}>Registration</button>
        <button onClick={this.goHome}>Go Home</button>
        <button onClick={this.reset}>reset localStorage</button>
        
      </div>
    );
  }
}

export default Login;
