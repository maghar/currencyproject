import React, { Component } from "react";
import { FormErrors } from "./FormErrors";

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      formErrors: { email: "", password: "" },
      emailValid: false,
      passwordValid: false,
      formValid: false
    };
  }

  handleUserInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  validateField = (fieldName, value) => {
    let fieldValidationErrors = this.state.formErrors;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;
    switch (fieldName) {
      case "email":
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailValid ? "" : " is invalid";
        break;
      case "password":
        passwordValid = value.length >= 6;
        fieldValidationErrors.password = passwordValid ? "" : " is too short";
        break;
      default:
        break;
    }
    this.setState(
      {
        formErrors: fieldValidationErrors,
        emailValid: emailValid,
        passwordValid: passwordValid
      },
      this.validateForm
    );
  };
  validateForm() {
    this.setState({
      formValid: this.state.emailValid && this.state.passwordValid
    });
  }
  errorClass(error) {
    return error.length === 0 ? "" : "has-error";
  }

  login = () => {
    this.props.history.push("/login");
  };

  registr = () => {
    localStorage.setItem("email", this.state.email);
    localStorage.setItem("password", this.state.password);
    this.props.history.push("/login");
  };

  render() {
    return (
      <div className="pt-4">
        <div className="d-flex justify-content-between">
          <h1>Registration</h1>
          <button
            type="button"
            onClick={this.login}
            className="btn btn-secondary btn-sm h-25"
          >
            login
          </button>
        </div>

        <div>
        <div className="panel panel-default">
          <FormErrors formErrors={this.state.formErrors} />
        </div>
          <form className="demoForm">
            <div className="form-group">
              <label htmlFor="email">Email address</label>
              <input
                value={this.state.email}
                onChange={this.handleUserInput}
                type="email"
                className="form-control"
                name="email"
              />
            </div>
            <div
              className={`form-group ${this.errorClass(
                this.state.formErrors.email
              )}`}
            />
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                value={this.state.password}
                onChange={this.handleUserInput}
                type="password"
                className="form-control"
                name="password"
              />
            </div>
            <button
              type="submit"
              className="btn btn-primary"
              disabled={!this.state.formValid}
              onClick={this.registr}
            >
              Sign up
            </button>
          </form>
        </div>
        
      </div>
    );
  }
}

export default Registration;
