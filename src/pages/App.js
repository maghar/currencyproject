import React, { Fragment } from "react";
import { Route } from "react-router";


const App = ({ routes, history }) => {
  
  function RouteWithSubRoutes(route) {
    return (
      <Route
        path={route.path}
        render={props => <route.component {...props} routes={route.routes} />}
      />
    );
  }
  

  return (
    <Fragment>
      <div className="container" history={history}>
        <div className="content">
          {routes.map(route => (
            <RouteWithSubRoutes key={route.path} {...route} />
          ))}
        </div>
      </div>
    </Fragment>
  );
};

export default App;
